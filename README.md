Symfony Test Task for Payever
=======

Technologies: Symfony 2.8

Entities:

* Album
* Images

### Installation

* `php app/console doctrine:database:create`
* `php app/console doctrine:schema:update`
* `php app/console doctrine:fixtures:load`

### Testing
* `phpunit -c app`
