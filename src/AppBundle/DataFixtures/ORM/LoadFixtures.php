<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\Purger;
use AppBundle\Entity\Image;
use AppBundle\Entity\Album;
use Faker;

class LoadFixtures implements FixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i < 5; $i++) {
            $album = new Album();
            $album->setTitle(ucwords(implode(' ', $faker->words())));
            $album->setDescription($faker->text);
            $manager->persist($album);
            $manager->flush();
            $this->generateImages($album, $manager, (($i*3)+1)*5);
        }
    }

    protected function generateImages($album, $manager, $n = 20)
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i < $n; $i++) {
            $image = new Image();
            $image->setTitle(ucwords(implode(' ', $faker->words())));
            $image->setDescription($faker->text);
            $image->setFileName("http://loremflickr.com/320/320?random=".rand());
            $image->setAlbum($album);
            $manager->persist($image);
        }
        $manager->flush();
    }
}
