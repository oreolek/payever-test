<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', '/album/1');
        $response = $client->getResponse();
        $this->assertTrue($client->getResponse()->isSuccessful(), 'response status is 2xx');
    }
}
