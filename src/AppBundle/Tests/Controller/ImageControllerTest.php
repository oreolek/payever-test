<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ImageControllerTest extends WebTestCase
{
    public function testRest()
    {
        $client = static::createClient();
        $id = 1;
        $client->request('GET', '/image/'.$id, [
            '_format' => 'json'
        ]);
        $response = $client->getResponse();
        $this->assertTrue($client->getResponse()->isSuccessful(), 'response status is 2xx');
        $content = $response->getContent();

        $decoded = json_decode($content, true);
        $this->assertNotEmpty($decoded);
        $this->assertTrue(isset($decoded['id']));
        $this->assertEquals($id, $decoded['id']);
    }
}
