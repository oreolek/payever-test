<?php
namespace AppBundle\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Extension;
use Twig_SimpleFilter;
class ImagepathTwigExtension extends Twig_Extension
{
    protected $container;
    public function __construct(ContainerInterface $container=null)
    {
        $this->container = $container;
    }

    protected function get($service)
    {
        return $this->container->get($service);
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('fieldnames', array($this, 'fieldnamesFilter')),
        );
    }

    public function getName() {
        return 'path_twig_extension';
    }

    public function fieldnamesFilter($filename) {
        if (strpos($filename, 'http://') !== FALSE) {
            return $filename;
        }
        $imagedir = $this->container->getParameter('upload_directory_pub');
        return $imagedir.'/'.$filename;
    }
}
